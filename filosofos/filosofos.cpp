#include <iostream>
#include <sstream>
#include <chrono>
#include <vector>
#include <thread>

int main(){

    std::vector<std::thread> philosophers;
    
    for (int i=0; i < 5; i++){
        std::thread philosopher([](int i)
                {std::ostringstream buffer; 
                buffer << "Process " << i << " has id: " 
                << std::this_thread::get_id() << std::endl;
                std::cout << buffer.str();
                }, i);
        philosophers.push_back(std::move(philosopher));
    }
    for (auto &philosopher: philosophers){
        philosopher.join();
    }
    std::cout << "Done" << std::endl;
}
