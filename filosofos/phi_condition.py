from multiprocessing import Process
from multiprocessing import Lock, Condition
from multiprocessing import current_process
from multiprocessing import Value, Array
import time
from random import random

N = 5
T = .5

def delay(factor = 3):
    time.sleep(random() / factor)

class Philosopher():
    def __init__(self, name, seat, table):
        self.name = name
        self.life = Process(target=self.philosopher_life )
        self.table = table
        self.seat = seat
        self.state = "Thinking"
        self.meals = Value('i', 0)

    def get_name(self):
        return self.name

    def is_manolo(self):
        return self.name == "Manolo"

    def get_seat(self):
        return self.seat
    
    def change_state(self, new_state):
        if self.state != new_state:
            self.state = new_state
            print(f"{self.name} is {self.state}")

    def philosopher_life(self):
        print(f"{self.name} is alive")
        while time.time() < self.table.end_time.value:
            self.think()
            self.eat()
    
    def eat(self):
        self.change_state("Hungry")
        self.table.request_forks(self)
        self.change_state("Eating")
        delay()
        self.meals.value += 1
        self.change_state("Thinking")
        self.table.deposit_forks(self)

    def think(self):
        self.change_state("Thinking")
        delay()
        if self.is_manolo():
            print(f"Me la suda, soy {self.name}")

    def __del__(self):
        self.life.join()
        self.table.log.append(f"{self.name} had {self.meals.value} meals")
        print(f"{self.name} died")

class Table():
    def __init__(self, capacity, amount_time):
        self.capacity = capacity
        self.mutex = Lock()
        self.forks = Array('i',[-1 for _ in range(self.capacity)])
        self.forks_available = Condition(self.mutex)
        self.commensals = []
        self.end_time = Value('f', time.time() + amount_time * 60)
        self.log = []
        print(f"Table is set for {capacity} philosophers and" + 
                "{amount_time} minutes")

    def are_being_used(self, fork1, fork2):
        def are_being_used_i_and_j():
            return self.forks[fork1]==-1 and self.forks[fork2]==-1
        return are_being_used_i_and_j
    
    def __del__(self):
        print(self.log)
    
    def __str__(self):
        self.mutex.acquire()
        output = "Dinners: "
        for commensal in self.commensals:
            output += f"{commensal.name} "
        output += "\n"
        self.mutex.release()
        return output

    def sit(self, name):
        self.mutex.acquire()
        seat = len(self.commensals)
        self.commensals.append(Philosopher(name,
            seat,
            self))
        print(f"{name} sat on the table at seat {seat}")
        self.mutex.release()

    def start_dinning(self):
        for commensal in self.commensals:
            commensal.life.start()

    def request_forks(self, philosopher):
        self.mutex.acquire()
        philosopher_id = philosopher.get_seat()
        left_fork_id = philosopher_id
        right_fork_id = (philosopher_id + 1) % self.capacity
        self.forks_available.wait_for(lambda: self.are_being_used(left_fork_id,
            right_fork_id))
        self.forks[left_fork_id] = philosopher_id
        self.forks[right_fork_id] = philosopher_id
        self.mutex.release()
    
    def deposit_forks(self, philosopher):
        self.mutex.acquire()
        philosopher_id = philosopher.get_seat()
        left_fork_id = philosopher_id
        right_fork_id = (philosopher_id + 1) % self.capacity
        self.forks[left_fork_id] = -1
        self.forks[right_fork_id] = -1
        self.forks_available.notify()
        self.mutex.release()

def main():
    table = Table(N,T)
    table.sit("Aristotle")
    table.sit("Kant")
    table.sit("Hypatia")
    table.sit("Plato")
    table.sit("Manolo")
    table.start_dinning()

if __name__ == "__main__":
    main()
