import time
import random

from multiprocessing import Process
from multiprocessing import current_process
def f(value):
    for i in range(3):
        print(f"hola soy {current_process().name}, " + \
              f"{current_process().pid}, " + \
              f"{current_process().is_alive()}, vuelta {i}")
        time.sleep(random.random()/3)
def g():
    print("adios")

if __name__ == "__main__":
    N = 10
    lp = []
    for i in range(N):
        lp.append(Process(target=f, args=(f"ana {i}",)))
# 1, es la tupla de un elemento
    for p in lp:
        p.start()
    q = Process(target=g)
    q.start()
    print("fin")
