from time import sleep
from random import random

from multiprocessing import Process

def f(name):
    for i in range(5):
        print("hola", i, name)
        sleep(random())

if __name__ == "__main__":
    p = Process(target=f, args=('soy p',))
    q = Process(target=f, args=('soy q',))
    p.start()
    q.start()
    print("fin")
