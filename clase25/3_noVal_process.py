from multiprocessing import Process
from multiprocessing import current_process
from multiprocessing import Value

class Counter(object):
    def __init__(self, initval=0):
        self.val = initval

    def increment(self):
       self.val += 1

def f(c):
    for i in range(100):
        c.increment()
        print(f"hola soy {current_process().pid} " + \
                f"vuelta: {i}, contador{c.val}")

if __name__ == "__main__":
    N = 8
    lp = []
    c = Counter()
    for i in range(N):
        lp.append(Process(target=f, args=(c,)))
    print("Valor inicial del contador", c.val)
    for p in lp:
        p.start()
