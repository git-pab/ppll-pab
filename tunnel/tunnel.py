from enum import Enum
from multiprocessing import Process
from multiprocessing import Value, Condition, Lock
import time, random

NC = 100;

def delay(d = 3):
    time.sleep(random.random()/d)

class Tunnel:
    def __init__(self, name):
        self.name = name
        self.mutex = Lock()
        self.is_safe = Condition(self.mutex)
        self.traffic = Condition(self.mutex)
        self.nwaiting_north = Value('i', 0)
        self.nwaiting_south = Value('i', 0)
        self.ndriving_north = Value('i', 0)
        self.ndriving_south = Value('i', 0)
        self.turn = Value('i', 0)
        self.car_destination = 0
        #turn 0 for northbound cars
        #turn 1 for southbound cars

    def no_front_collision_risk(self):
        return ((self.ndriving_north.value == 0 and self.turn.value == 1) or
                (self.ndriving_south.value == 0 and self.turn.value == 0))
    
    def no_traffic_south(self):
        return self.nwaiting_south.value == 0
        
    def no_traffic_north(self):
        return self.nwaiting_north.value == 0

    def northbound_arrival(self):
        self.traffic.wait_for(self.no_traffic_south)
        self.nwaiting_north.value += 1
        self.turn.value = 0
        self.is_safe.wait_for(self.no_front_collision_risk)
        self.nwaiting_north.value -= 1
        self.traffic.notify_all()
        self.ndriving_north.value += 1
    
    def southbound_arrival(self):
        self.traffic.wait_for(self.no_traffic_north)
        self.nwaiting_south.value += 1
        self.turn.value = 1
        self.is_safe.wait_for(self.no_front_collision_risk)
        self.nwaiting_south.value -= 1
        self.traffic.notify_all()
        self.ndriving_south.value += 1

    def car_arrival(self, car_destination):
        self.mutex.acquire()
        self.car_destination = car_destination
        if self.car_destination == 0:
            self.northbound_arrival()
        else:
            self.southbound_arrival()
        self.mutex.release()

    def northbound_departure(self):
        self.ndriving_north.value -= 1
        if self.ndriving_north.value == 0:
            self.turn.value = 1
            self.is_safe.notify()

    def southbound_departure(self):
        self.ndriving_south.value -= 1
        if self.ndriving_south.value == 0:
            self.turn.value = 0
            self.is_safe.notify()

    def car_departure(self, car_destination):
        self.mutex.acquire()
        self.car_destination = car_destination
        if self.car_destination == 0:
            self.northbound_departure()
        else:
            self.southbound_departure()
        self.is_safe.notify_all()
        self.mutex.release()

    def __str__(self):
        if self.turn.value == 0:
            return f"state: northbound traffic {self.n_driving_north.value}"
        else:
            return f"state: southbound traffic {self.n_driving_south.value}"

def driving_north(tunnel):
    direction = 0
    destination = "north"
    print(f"car going {destination} wants to enter the tunnel")
    tunnel.car_arrival(direction)
    print(f"car is driving to the {destination} inside the tunnel")
    delay()
    print(f"car passed the tunnel going {destination}")
    tunnel.car_departure(direction)

def driving_south(tunnel):
    direction = 1
    destination = "south"
    print(f"car going {destination} wants to enter the tunnel")
    tunnel.car_arrival(direction)
    print(f"car is driving to the {destination} inside the tunnel")
    delay()
    print(f"car passed the tunnel going {destination}")
    tunnel.car_departure(direction)

def driving(tunnel):
    direction = random.choices([0, 1], weights = [1, 1])[0]
    if direction == 0:
        driving_north(tunnel)
    else:
        driving_south(tunnel)

def main():

    tunnel = Tunnel("tunel japo")
    
    cars = [Process(target=driving, args=(tunnel,)) for _ in range(NC)]

    for car in cars:
        delay()
        car.start()

    for car in cars:
        car.join()

if __name__ == '__main__':
    main()
