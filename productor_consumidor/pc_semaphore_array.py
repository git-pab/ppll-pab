from multiprocessing import Process
from multiprocessing import Semaphore, BoundedSemaphore
from multiprocessing import current_process
from multiprocessing import Value, Array
from time import sleep
from random import random

N = 10
K = 3

def delay(factor = 3):
    sleep(random()/factor)

def p(storage, index, non_full, non_empty):
    for v in range(N):
        print (current_process().name, "produciendo")
        delay(6)
        non_full.acquire()
        storage[index.value] = v
        delay(6)
        index.value = index.value + 1
        non_empty.release()
        print (current_process().name, "storageado", v)


def c(storage, index, non_full, non_empty):
    for v in range(N):
        non_empty.acquire()
        print (current_process().name, "desstorageando")
        dato = storage[0]
        index.value = index.value - 1
        delay()
        for i in range(index.value):
            storage[i] = storage[i + 1]
        storage[index.value] = -1
        non_full.release()
        print (current_process().name, "consumiendo", dato)
        delay()

def main():
    storage = Array('i', [-1 for _ in range(K)])
    index = Value('i',0)

    print ("storage inicial", storage[:], "index", index.value)


    mutex = Semaphore(1)
    non_empty = Semaphore(0)
    non_full = BoundedSemaphore(K)

    productor = Process(target=p, name="productor", args=(storage, index, non_full, non_empty))

    consumidor = Process(target=c, name="consumidor", args=(storage, index, non_full, non_empty))

    consumidor.start()
    productor.start()


if __name__ == "__main__":
    main()
