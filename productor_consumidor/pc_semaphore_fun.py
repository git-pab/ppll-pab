from multiprocessing import Process
from multiprocessing import Semaphore
from multiprocessing import current_process
from multiprocessing import Value, Array
from time import sleep
from random import random

N = 10
K = 3

def delay(factor = 3):
    sleep(random()/factor)

def add_data(storage, index, data, mutex):
        mutex.acquire()
        storage[index.value] = data
        delay(6)
        index.value = index.value + 1
        mutex.release()

def get_data(storage, index, mutex):
    mutex.acquire()
    data = storage[0]
    index.value = index.value - 1
    delay()
    for i in range(index.value):
        storage[i] = storage[i + 1]
    storage[index.value] = -1
    mutex.release()
    return data

def p(storage, index, empty, non_empty, mutex):
    for v in range(N):
        print (current_process().name, "produciendo")
        delay(6)
        empty.acquire()
        add_data(storage, index, v, mutex)
        non_empty.release()
        print (current_process().name, "almacenado", v)


def c(storage, index, empty, non_empty, mutex):
    for v in range(N):
        non_empty.acquire()
        print (current_process().name, "desalmacenando")
        dato =get_data(storage, index, mutex)
        empty.release()
        print (current_process().name, "consumiendo", dato)
        delay()

if __name__ == "__main__":

    storage = Array('i', K)
    index = Value('i',0)
    for i in range(K):
        storage[i] = -1
    print ("almacen inicial", storage[:], "indice", index.value)


    mutex = Semaphore(1)
    non_empty = Semaphore(0)
    empty = Semaphore(K)

    productor = Process(target=p, name="productor", args=(storage, index, empty, non_empty, mutex))

    consumidor = Process(target=c, name="consumidor", args=(storage, index, empty, non_empty, mutex))

    consumidor.start()
    productor.start()
