from multiprocessing import Process
from multiprocessing import Semaphore, BoundedSemaphore
from multiprocessing import current_process
from multiprocessing import Value
from time import sleep
from random import random

N = 10

def p(storage, non_full, non_empty):
    for v in range(N):
        print (current_process().name, "produciendo")
        sleep(random()/3)
        non_full.acquire()
        storage.value = v
        non_empty.release()
        print (current_process().name, "storageado", v)


def c(storage, non_full, non_empty):
    for _ in range(N):
        non_empty.acquire()
        print (current_process().name, "desstorageando")
        dato = storage.value
        storage.value = -1
        non_full.release()
        print (current_process().name, "consumiendo", dato)
        sleep(random()/3)

def main():
    storage = Value('i', -1)
    print ("storage inicial", storage.value)

    non_empty = Semaphore(0)
    non_full = BoundedSemaphore(1)

    productor = Process(target=p, name="productor", args=(storage, non_full, non_empty))

    consumidor = Process(target=c, name="consumidor", args=(storage, non_full, non_empty))

    consumidor.start()
    productor.start()

if __name__ == "__main__":
    main()
