from multiprocessing import Process
from multiprocessing import Lock, Semaphore, BoundedSemaphore, Condition
from multiprocessing import current_process
from multiprocessing import Value, Array
from time import sleep
from random import random

N = 10
K = 3

def delay(factor = 3):
    sleep(random()/factor)


class Storage(object):
    def __init__(self, k):
        self.index = Value('i', 0)
        self.storage = Array('i', [-1 for _ in range(K)])
        self.mutex = Lock()
        self.non_full = Condition(self.mutex)
        self.non_empty = Condition(self.mutex)

    def is_there_room(self):
        return self.index.value<len(self.storage)

    def add_element(self, data):
        self.mutex.acquire()
        self.non_full.wait_for(self.is_there_room)
        self.storage[self.index.value] = data
        delay(60)
        self.index.value = self.index.value + 1
        self.non_empty.notify()
        self.mutex.release()

    def is_not_empty(self):
        return self.index.value>0

    def get_element(self):
        self.mutex.acquire()
        self.non_empty.wait_for(self.is_not_empty)
        dato = self.storage[0]
        self.index.value = self.index.value - 1
        delay(3)
        for i in range(self.index.value):
            self.storage[i] = self.storage[i + 1]
        self.storage[self.index.value] = -1
        self.non_full.notify()
        self.mutex.release()
        return dato

    def __str__(self):
        return f"<{self.storage[:]}, {self.index.value}>"


def p(storage):
    for v in range(1,N+1):
        print(f"Adding element {v}")
        storage.add_element(v)
        print(f"Element added {storage}")
        delay(60)

def c(storage):
    for _ in range(1,N+1):
        print(f"Getting element")
        v = storage.get_element()
        print(f"Element consumed {v}")
        delay(3)


def main():
    storage = Storage(K)
    print(f"initial storage: {storage}")

    productor = Process(target=p, name="productor", args=(storage, ))

    consumidor = Process(target=c, name="consumidor", args=(storage, ))

    consumidor.start()
    productor.start()

if __name__ == "__main__":
    main()
