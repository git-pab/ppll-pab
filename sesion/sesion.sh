#!/bin/bash

wget http://dana.estad.ucm.es/gead17/inf-mat_17-18.tgz
tar xzf inf-mat_17-18.tgz
cd inf-mat_17-18
ls -al #a para ficheros ocultos, l para formato largo con mas informacion.
ls examenes
rm just*
rm -rf .git #f para que no pregunte si encuentra ficheros sin permisos etc.
cp -r ejemplos ejemplos_new
cd ejemplos_new
for i in /..ejemplos ./; do cp ../escudo.png $i; done #copiar en ambos sitios.
grep radius *.py
cd ../
find . | grep -P '$tex' #tambien se puede usar find -name "*tex".
#el uso de -P es para poder usar expresiones regulares, $ es acabado en. 
rm -r `du -s * | sort -n | tail -1 | grep -oP '[0-9]*\s*\K\w+'`
#du -s * te da la lista de ficheros, la ordenamos con un sort numerico.
#nos quedamos con la ultima linea y nos quedamos con el nombre del directorio.
#el -o de grep hace que saque la parte que encaja con la expresion regular.
#el \K de la expresion regular es para capturar la parte tras la expresion.
# rm -r `du -s * | sort -n | tail -1 | awk '{print $2}'` es una alternativa.
head -20 ene2018.tex
tail -20 ene2018.tex
cd ../../../
pwd
